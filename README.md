## SimplyTranslate
A privacy friendly frontend to multiple Translation Engines.

### History
1. SimplyTranslate was founded by metalune and fattalion. It was written in python.
2. Fattalion created a Go implementation.
3. Both metalune and fattalion retired, and they handed SimplyTranslate over to ManeraKai.